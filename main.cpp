#include <iostream>
#include <string>
#include <vector>
#include <complex>
#include "libppm/libppm.h"
#include "libppm_wrapper.hpp"

// This is more C with extras then C++...


void show_helppage() {
    std::cout << "By DasUngesagte, Licensed under GPL3" << std::endl;
    std::cout << "Image parameters:" << std::endl;
    std::cout << "\t -i [INPUT 1-CHANNEL PPM] " << std::endl;
    std::cout << "Blur parameters:" << std::endl;
    std::cout << "\t -s [SIGMA] " << std::endl;
    std::cout << "Non-Maximum-Suppression parameters:" << std::endl;
    std::cout << "\t none " << std::endl;
    std::cout << "Hysteresis parameters:" << std::endl;
    std::cout << "\t -Tl [THRESHOLD LOW] " << std::endl;
    std::cout << "\t -Th [THRESHOLD HIGH] " << std::endl;
    std::cout << "Hough parameters:" << std::endl;
    std::cout << "\t -ro [RO STEPS] " << std::endl;
    std::cout << "\t -l [L STEPS] " << std::endl;
    std::cout << "Optional arguments:" << std::endl;
    std::cout << " -o [OUTPUT PPM]" << std::endl;
}

std::string deleteFileExtension(std::string extension){
    // TODO
    return std::string("");
}

void desaturate(uint8_t *source, int64_t image_width, int64_t image_height){
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t idx = (y * image_width + x) * 3;
            uint8_t r_px = source[idx + 0];
            uint8_t g_px = source[idx + 1];
            uint8_t b_px = source[idx + 2];

            auto grey_val = (uint8_t) std::round(0.22 * r_px + 0.72 * g_px + 0.06 * b_px) ;

            source[idx + 0] = grey_val;
            source[idx + 1] = grey_val;
            source[idx + 2] = grey_val;
        }
    }
}

void makeSingleChannel(uint8_t const *source, uint8_t *target, int64_t image_width, int64_t image_height) {
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t idx = y * image_width + x;
            target[idx] = source[idx * 3];
        }
    }
}

void convolute2D(uint8_t const *source, double *target, int64_t image_width, int64_t image_height, double const *filter,
                 int64_t filter_size) {

    int64_t filter_radius = std::floor(filter_size / 2);

    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {

            double pix = 0.0;

            // Convolute:
            for (int64_t idx_x = 0; idx_x < filter_size; ++idx_x) {

                // Edge reflection for x-axis:
                int64_t fil_px_idx_x = x + idx_x - filter_radius;
                if (fil_px_idx_x < 0) {
                    fil_px_idx_x = 0;
                } else if (fil_px_idx_x >= image_width) {
                    fil_px_idx_x = image_width - 1;
                }

                for (int64_t idx_y = 0; idx_y < filter_size; ++idx_y) {

                    // Edge reflection for y-axis:
                    int64_t fil_px_idx_y = y + idx_y - filter_radius;
                    if (fil_px_idx_y < 0) {
                        fil_px_idx_y = 0;
                    } else if (fil_px_idx_y >= image_height) {
                        fil_px_idx_y = image_height - 1;
                    }

                    int64_t fil_px_idx = fil_px_idx_y * image_width + fil_px_idx_x;  // 1-D index
                    double fil_pix = source[fil_px_idx] * 1.0;                      // Load pix as double
                    pix += fil_pix * filter[idx_y * filter_size + idx_x];           // Accumulate
                }
            }

            int64_t cur_px_idx = y * image_width + x;
            target[cur_px_idx] = pix;
        }
    }
}

void convolute1D(uint8_t const *source, uint8_t *target, int64_t image_width, int64_t image_height, double const *filter,
                 int64_t filter_size) {

    int64_t filter_radius = std::floor(filter_size / 2);
    auto tmp_target = new uint8_t[image_height * image_width];     // Single channel temporary target for 1st pass

    // Horizontal pass:
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            double pix = 0.0;

            // Convolute:
            for (int64_t idx = 0; idx < filter_size; ++idx) {

                // Edge reflection for x-axis:
                int64_t fil_px_idx = x + idx - filter_radius;
                if (fil_px_idx < 0) {
                    fil_px_idx = 0;
                } else if (fil_px_idx >= image_width) {
                    fil_px_idx = image_width - 1;
                }

                fil_px_idx = y * image_width + fil_px_idx;  // 1-D index
                double fil_pix = source[fil_px_idx] * 1.0;  // Load pix as double
                pix += fil_pix * filter[idx];               // Accumulate

            }

            int64_t cur_px_idx = y * image_width + x;
            tmp_target[cur_px_idx] = (uint8_t) std::round(std::abs(pix));
        }
    }

    // Vertical pass:
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            double pix = 0.0;

            // Convolute:
            for (int64_t idx = 0; idx < filter_size; ++idx) {

                // Edge reflection for y-axis:
                int64_t fil_px_idx = y + idx - filter_radius;
                if (fil_px_idx < 0) {
                    fil_px_idx = 0;
                } else if (fil_px_idx >= image_height) {
                    fil_px_idx = image_height - 1;
                }

                fil_px_idx = fil_px_idx * image_width + x;              // 1-D index, without CHANNEL
                double fil_pix = tmp_target[fil_px_idx] * 1.0;          // Load pix as double
                pix += fil_pix * filter[idx];                           // Accumulate

            }

            auto val = (uint8_t) std::round(std::abs(pix));
            target[y * image_width + x] = val;
        }
    }

    delete[] tmp_target;
}

void createGaussianFilter1D(double filter[], int64_t size, double sigma) {

    double s = 2.0f * sigma * sigma;
    double factor = 1 / static_cast<double>(sqrt(2 * M_PI * sigma * sigma));
    int64_t mean = (int64_t) size / 2;

    // Generate the filter:
    for (int64_t x = 0; x < size; x++) {

        // The middle of the matrix is (0,0), so shift the indices:
        double x1 = static_cast<float>(x) - (double) mean;
        double r = x1 * x1;
        filter[x] = factor * std::exp(-r / s);
    }
}

void gaussianBlur(uint8_t const *source, uint8_t *target, int64_t image_width, int64_t image_height, float sigma) {

    // Create kernel:
    int64_t filter_size = (int64_t) std::ceil(6 * sigma) | 1; // 6 times sigma, next odd.
    std::cout << "Gaussian Filter size: " << filter_size << std::endl;
    double filter[filter_size];
    createGaussianFilter1D(filter, filter_size, sigma);
    convolute1D(source, target, image_width, image_height, filter, filter_size);
}

void sobelFilter(uint8_t const *source, int64_t image_width, int64_t image_height, uint8_t *sobel_comb, double *sobel_dir) {

    double sobel_fil_x[3][3] = {{-1, 0, 1},
                                {-2, 0, 2},
                                {-1, 0, 1}};    // + and - are mirrored for some reason...
    double sobel_fil_y[3][3] = {{1,  2,  1},
                                {0,  0,  0},
                                {-1, -2, -1}};

    auto sobel_x = new double[image_width * image_height];
    auto sobel_y = new double[image_width * image_height];

    convolute2D(source, sobel_x, image_width, image_height, reinterpret_cast<const double *>(sobel_fil_x), 3);
    convolute2D(source, sobel_y, image_width, image_height, reinterpret_cast<const double *>(sobel_fil_y), 3);

    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t idx = y * image_width + x;
            auto val = (uint8_t) std::round(std::sqrt(sobel_x[idx] * sobel_x[idx] + sobel_y[idx] * sobel_y[idx]));
            sobel_comb[idx] = val;
            sobel_dir[idx] = std::atan2(sobel_x[idx], sobel_y[idx]) + M_PI;   // Negative values possible
        }
    }

    delete[] sobel_x;
    delete[] sobel_y;
}

int checkClampedDirection(double deg, double axis) {
    double clamp_radius = 45.0 / 2;

    if((deg < 360.0 - clamp_radius && deg >= axis - clamp_radius && deg <= axis + clamp_radius) ||              // No wraparound with current angle
    (deg >= 360.0 - clamp_radius && deg >= axis + 360.0 - clamp_radius && deg <= axis + 360.0 + clamp_radius))  // In case of wrap around with angle
        return 1;

    return 0;
}

void nonMaximumSuppression(uint8_t const *source, int64_t image_width, int64_t image_height, double const *sobel_dir,
                           uint8_t *target) {

    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {

            int64_t idx = y * image_width + x;
            double deg = 180.0 * sobel_dir[idx] / M_PI;
            uint8_t pix = source[idx];
            int64_t p_idx = -1;
            int64_t q_idx = -1;

            // Outline directions are quantised in eight steps:
            if (checkClampedDirection(deg, 0) || checkClampedDirection(deg, 180)) {          // 0° axis
                p_idx = y - 1 >= 0 ? (int64_t) ((y - 1) * image_width + x) : -1;
                q_idx = y + 1 < image_height ? (int64_t) ((y + 1) * image_width + x) : -1;
            } else if (checkClampedDirection(deg, 45) || checkClampedDirection(deg, 225)) {  // 45° axis
                p_idx = (y - 1 >= 0) && (x + 1 < image_width) ? (int64_t) ((y - 1) * image_width + x + 1) : -1;
                q_idx = (y + 1 < image_height) && (x - 1 >= 0) ? (int64_t) ((y + 1) * image_width + x - 1) : -1;
            } else if (checkClampedDirection(deg, 90) || checkClampedDirection(deg, 270)) {  // 90° axis
                p_idx = x - 1 >= 0 ? (int64_t) (y * image_width + x - 1) : -1;  // p
                q_idx = x + 1 < image_width ? (int64_t) (y * image_width + x + 1) : -1;  // q
            } else if (checkClampedDirection(deg, 135) || checkClampedDirection(deg, 315)) { // 135° axis
                p_idx = (y - 1 >= 0) && (x - 1 >= 0) ? (int64_t) ((y - 1) * image_width + x - 1) : -1;
                q_idx = (y + 1 < image_height) && (x + 1 < image_width) ? (int64_t) ((y + 1) * image_width + x + 1) : -1;
            } else {
                std::cerr << "Clamping-error! deg = " << deg << std::endl;
            }

            // If pixel is out of bounds, assume a value of 0:
            uint8_t upper_pix = p_idx < 0 ? 0 : source[p_idx];
            uint8_t lower_pix = q_idx < 0 ? 0 : source[q_idx];

            // If one pixel has a greater value, our pixel will be set to 0:
            target[idx] = pix < upper_pix || pix < lower_pix ? 0 : pix;
        }
    }
}

void hysteresisThresholding(uint8_t *source, int64_t image_width, int64_t image_height, int64_t t_high, int64_t t_low){

    if(t_high > 4 * t_low || t_high < 2 * t_low){
        std::cout << "Possibly non-optimal Th: keep between 2 * Tl < Th < 4 * Tl" << std::endl;
        std::cout << "Continuing..." << std::endl;
    }

    bool change = true;

    // Set all over t_high to maximum:
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t idx = y * image_width + x;
            if (source[idx] >= t_high) {
                source[idx] = 255;
            }
        }
    }

    // Set pixel over t_low high, if a high pixel is neighbouring:
    while(change){
        change = false;
        for (int64_t y = 0; y < image_height; ++y) {
            for (int64_t x = 0; x < image_width; ++x) {

                int64_t idx = y * image_width + x;

                if(source[idx] >= t_low && source[idx] < t_high){
                    for(int8_t nn_x = -1; nn_x < 2; ++nn_x){
                        for(int8_t nn_y = -1; nn_y < 2; ++nn_y){

                            // OOB check:
                            int64_t x_oob = x + nn_x >= image_width || x + nn_x < 0;
                            int64_t y_oob = y + nn_y >= image_height || y + nn_y < 0;
                            uint8_t val;
                            if(x_oob || y_oob)
                                val = 0;
                            else
                                val = source[(y + nn_y) * image_width + x + nn_x];
                            if(val == 255){
                                source[idx] = 255;
                                change = true;
                                nn_x = 42; nn_y = 42; // break out of both loops
                            }
                        }
                    }
                }
            }
        }
    }

    // Everything to zero, what is not at max:
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t idx = y * image_width + x;
            if (source[idx] < 255) {
                source[idx] = 0;
            }
        }
    }
}

void houghTransform(uint8_t *original, uint8_t const *sobel_strength, uint8_t const *sobel_dir, uint8_t const *hysteresis, int64_t ro_steps, int64_t l_steps, int64_t image_width, int64_t image_height){

    // Discretization:
    double l_max = std::sqrt(image_width * image_width + image_height * image_height); // Diagonally through the image
    double l_step = l_max / (double) l_steps;   // Value of each step
    std::cout << "l_max = " << l_max << std::endl;
    std::cout << "l value per pixel = " << l_step << std::endl;
    std::cout << "ro value per pixel = " << 2.0 * M_PI / (double) ro_steps << std::endl;

    auto H = new int64_t[ro_steps * l_steps];

    /**************************************************************************
     *                          Building the Hough Space                      *
     **************************************************************************/

    // Transform every edge-point xi, yi via l = xi cos ro + yi sin ro from the xy-space to the ro-l-space
    for(int64_t ro_idx = 0; ro_idx < ro_steps; ++ro_idx){
        for(int64_t l_idx = 0; l_idx < l_steps; ++l_idx){
            H[l_idx * ro_steps + ro_idx] = 0;
        }
    }

    // Calculate bins:
    for (int64_t y = 0; y < image_height; ++y) {
        for (int64_t x = 0; x < image_width; ++x) {
            int64_t pix_idx = y * image_width + x;
            if (hysteresis[pix_idx] == 255) {   // Is edge point
                for (int64_t ro_idx = 0; ro_idx < ro_steps; ++ro_idx) {
                    double ro = 2.0 * M_PI * (double) ro_idx / (double) ro_steps; // Get ro back from the discretization
                    double l = (double) x * std::cos(ro) + (double) y * std::sin(ro);

                    // Ignore lines 180 degs to the current one...
                    if(l >= 0.0){
                        // Round l to next value with l-step to get our discretized l index:
                        int64_t l_idx = std::floor(l / l_step);
                        H[l_idx * ro_steps + ro_idx] += sobel_strength[pix_idx];
                    }

                }
            }
        }
    }

    /**************************************************************************
     *                               Print Hough                              *
     **************************************************************************/
    uint64_t x_max = 0;
    uint64_t y_max = 0;

    // Find absolute maximum, to stretch every pixel back into 0-255:
    int64_t max = 0;
    for(int64_t ro_idx = 0; ro_idx < ro_steps; ++ro_idx){
        for(int64_t l_idx = 0; l_idx < l_steps; ++l_idx){
            if(H[l_idx * ro_steps + ro_idx] > max){
                max = H[l_idx * ro_steps + ro_idx];
                x_max = ro_idx;
                y_max = l_idx;
            }
        }
    }

    std::cout << " Maximum at (" << x_max << ", " << y1 << ") with l = " << (double) y_max * l_step << " and ro = " << 180.0 * (2.0 * M_PI * (double) x_max / (double) ro_steps) / M_PI << "°" << std::endl;

    uint8_t H_image[ro_steps * l_steps];
    for(int64_t ro_idx = 0; ro_idx < ro_steps; ++ro_idx){
        for(int64_t l_idx = 0; l_idx < l_steps; ++l_idx){
            int64_t idx = l_idx * ro_steps + ro_idx;
            H_image[idx] = (uint8_t) std::round(255.0 * (double) H[idx] / (double) max);
        }
    }

    saveImage(H_image, ro_steps, l_steps, P2, "hough.ppm");


    /**************************************************************************
     *                            Finding Maximums                            *
     **************************************************************************/

    // Find maximums:
    int64_t threshold = max / 2;



    /**************************************************************************
     *                            Painting Lines                              *
     **************************************************************************/
    // Paint lines: // TODO: currently only one
    for (int64_t y = 0; y < image_height; ++y) {
        double ro = (2.0 * M_PI * (double) x_max / (double) ro_steps);
        double l = (double) y_max * l_step;
        double val = l - (double) y * std::sin(ro);
        double cos_x = std::cos(ro);
        if(cos_x < 0.0000000000000000000000000001 && cos_x > -0.0000000000000000000000000001){
            // 0 Fall
        } else {
            auto x_pos = (int64_t) std::round(val / cos_x);
            if(x_pos < image_width && x_pos >= 0){
                int64_t px_idx = (y * image_width + x_pos) * 3;
                original[px_idx + 0] = 0;
                original[px_idx + 1] = 255;
                original[px_idx + 2] = 0;
            }
        }
    }
}

int main(int argc, char *argv[]) {

    std::string input_image_path;
    std::string output_image_path;

    float sigma;
    int64_t t_low;
    int64_t t_high;
    int64_t ro_steps;
    int64_t l_steps;

    if (argc < 5) {
        std::cerr << "Too few arguments" << std::endl;
        show_helppage();
        return 0;
    }

    // Get the arguments:
    for (int64_t i = 1; i < argc; ++i) {

        std::string cur = std::string(argv[i]);

        if (cur == "-i") {
            ++i;
            input_image_path = std::string(argv[i]);
            output_image_path = std::string(argv[i]).append("_out.png");
        } else if (cur == "-o") {
            ++i;
            output_image_path = std::string(argv[i]);
        } else if (cur == "-s") {
            ++i;
            sigma = std::stof(argv[i]);
        } else if (cur == "-Tl") {
            ++i;
            t_low = std::stoll(argv[i]);
        } else if (cur == "-Th") {
            ++i;
            t_high = std::stoll(argv[i]);
        } else if (cur == "-ro") {
            ++i;
            ro_steps = std::stoll(argv[i]);
        } else if (cur == "-l") {
            ++i;
            l_steps = std::stoll(argv[i]);
        } else {
            show_helppage();
            return 0;
        }
    }

    std::vector<uint8_t> source_image;
    int64_t image_width;
    int64_t image_height;

    int channels = loadImage(source_image, image_width, image_height, input_image_path);

    auto target_image = new uint8_t[image_width * image_height];

    /**************************************************************************
     *                          1. Gaussian Blur                              *
     **************************************************************************/

    if (channels == 3) {
        auto source_image_single = new uint8_t[image_width * image_height];
        desaturate(&source_image[0], image_width, image_height);
        makeSingleChannel(&source_image[0], source_image_single, image_width, image_height);
        gaussianBlur(source_image_single, target_image, image_width, image_height, sigma);
        delete[] source_image_single;
    } else {
        gaussianBlur(&source_image[0], target_image, image_width, image_height, sigma);
    }

    // Do stuff
    saveImage(target_image, image_width, image_height, P2, "blur.ppm");

    /**************************************************************************
     *                          2. Sobel Operator                             *
     **************************************************************************/

    auto sobel_comb = new uint8_t[image_width * image_height];
    auto sobel_dir = new double[image_width * image_height];

    sobelFilter(target_image, image_width, image_height, sobel_comb, sobel_dir);
    saveImage(sobel_comb, image_width, image_height, P2, "sobel.ppm");

    delete[] target_image;

    /**************************************************************************
     *                       3. Non-Maximum Suppression                       *
     **************************************************************************/

    auto sobel_nms = new uint8_t[image_width * image_height];
    nonMaximumSuppression(sobel_comb, image_width, image_height, sobel_dir, sobel_nms);
    saveImage(sobel_nms, image_width, image_height, P2, "nms.ppm");

    /**************************************************************************
     *                       4. Hysteresis Thresholding                       *
     **************************************************************************/

    hysteresisThresholding(sobel_nms, image_width, image_height, t_high, t_low);
    saveImage(sobel_nms, image_width, image_height, P2, output_image_path);

    /**************************************************************************
     *                       5. Hough Transform (lines)                       *
     **************************************************************************/

    houghTransform(&source_image[0], sobel_comb, reinterpret_cast<const uint8_t *>(sobel_dir), sobel_nms, ro_steps, l_steps, image_width, image_height);

    saveImage(&source_image[0], image_width, image_height, P3, output_image_path);

    delete[] sobel_nms;
    delete[] sobel_comb;
    delete[] sobel_dir;

    return 0;
}