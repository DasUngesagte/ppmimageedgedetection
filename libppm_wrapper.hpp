#ifndef CUDA_VORONOI_LIBPPM_WRAPPER_HPP
#define CUDA_VORONOI_LIBPPM_WRAPPER_HPP

#include <cstdint>
#include <string>
#include <vector>
#include "libppm/libppm.h"

/**
 * Loads the image at the given path into the vector in a RGB-pattern
 */
int loadImage(std::vector<uint8_t> &image_source,
              int64_t &image_width,
              int64_t &image_height,
              const std::string &input_path) {

    PPM_Image *image = loadPPM(input_path.c_str());
    image_width = image->image_width;
    image_height = image->image_height;
    int channels = 1;
    if (image->format == P3 | image->format == P6 | image->format == P7)
        channels = 3;
    image_source = std::vector<uint8_t>(image->data, image->data + image_width * image_height * channels);
    deletePPM(image);
    return 3;
}


/**
 * Saves the image at the given path into the vector in a RGB-pattern
 */
void saveImage(const uint8_t *image_source,
               const size_t image_width,
               const size_t image_height,
               const Format format,
               const std::string &output_path) {

    PPM_Image *image = createPPM(format, image_width, image_height, 255, (unsigned char *) image_source);
    savePPM(output_path.c_str(), image);
    deletePPM(image);

}

#endif //CUDA_VORONOI_LIBPPM_WRAPPER_HPP